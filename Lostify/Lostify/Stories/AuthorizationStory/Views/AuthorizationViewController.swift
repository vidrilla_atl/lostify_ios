//
//  AuthorizationViewController.swift
//  Lostify
//
//  Created by Denis Gavrilenko on 10/24/16.
//  Copyright © 2016 DreamTeam. All rights reserved.
//

import UIKit

class AuthorizationViewController: UIViewController {
    final var router: AuthorizationRouter!
    
    
    @IBAction func onLogin(_ sender: AnyObject) {
        router.navigateToMain(view: self)
    }
}
